var mongoose = require("mongoose");
var GroceryItem = require("./models/GroceryItem.js");

mongoose.connect("mongodb://localhost:33017/grocery", function() {
   console.log("Connected");

   mongoose.connection.db.dropDatabase();

   var items = [{
      name: "ice cream"
      }, {
      name: "waffles"
      }, {
      name: "candy",
      purchased: true
      }, {
      name: "snarks"
   }];

   items.forEach(function(item){
      new GroceryItem(item).save();
   })
   
})


