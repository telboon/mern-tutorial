var mongoose = require('mongoose');

var GroceryItemSchema = {
   name: String,
   purchased: Boolean,
   id: String
};

GroceryItem= mongoose.model('GroceryItem', GroceryItemSchema, "groceryItems");

module.exports = GroceryItem;
